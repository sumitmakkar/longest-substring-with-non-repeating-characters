#include <iostream>
#include <vector>

using namespace std;

class Engine
{
    public:
        string findLongestSubsequenceWithNonRepeatingChaqractersInString(string str)
        {
            int    currentLength = 1;
            int    maxLength     = 1;
            int    endRange      = 0;
            int    len           = (int)str.length();
            int    previousIndex;
            string subsequenceString = "";
            vector<int> alpha(26);
            for(int i = 0 ; i < 26 ; i++)
            {
                alpha[i] = -1;
            }
            alpha[0] = 0;
            for(int i = 1 ; i < len ; i++)
            {
                previousIndex = alpha[str[i]-'A'];
                if(previousIndex == -1 || i - currentLength > previousIndex)
                {
                    currentLength++;
                }
                else
                {
                    if(maxLength < currentLength)
                    {
                        maxLength = currentLength;
                        endRange  = i - 1;
                    }
                    currentLength = i - previousIndex;
                }
                alpha[str[i]-'A'] = i;
            }
            
            for(int i = 0 ; i < maxLength ; i++)
            {
                subsequenceString += str[i + endRange - maxLength + 1];
            }
            return subsequenceString;
        }
};

int main(int argc, const char * argv[])
{
    string str = "ABCDABDEFGCABD";
    Engine e   = Engine();
    cout<<e.findLongestSubsequenceWithNonRepeatingChaqractersInString(str)<<endl;
    return 0;
}
